//
//  ItemDetailViewController.m
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ItemDetailViewController.h"

@interface ItemDetailViewController ()

@end

@implementation ItemDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Todo Item";
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Paper"]];
	self.titleLabel.text = self.item.title;
	[self.titleLabel setNumberOfLines:0];
	self.descriptionLabel.text = self.item.theDescription;
	iOS7 ? [self setAutomaticallyAdjustsScrollViewInsets:NO] : nil;

	self.doneImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, kScreenHeight / 2.0 - kScreenWidth / 2.0, kScreenWidth, kScreenWidth)];
	self.doneImage.backgroundColor = [UIColor clearColor];
	self.doneImage.image = [UIImage imageNamed:@"Done"];
	self.doneImage.alpha = 0.0;
	[self.view addSubview:self.doneImage];

	self.itemImage.contentMode = UIViewContentModeScaleAspectFit;
	[self.itemImage setImage:[UIImage imageWithData:self.item.image]];

	if (self.item.isComplete) {
		self.doneImage.alpha = 1.0;
	}
	else {
		self.doneImage.alpha = 0.0;
	}

	UIButton *zoomButton = [UIButton buttonWithType:UIButtonTypeCustom];
	zoomButton.frame = self.itemImage.frame;
	[zoomButton addTarget:self action:@selector(imageZoom) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:zoomButton];
	[self.view bringSubviewToFront:self.doneImage];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (IBAction)completePressed:(id)sender {
	if (!self.item.isComplete) {
		self.item.isComplete = YES;
		self.doneImage.alpha = 1.0;
	}
	else {
		self.item.isComplete = NO;
		self.doneImage.alpha = 0.0;
	}
	[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (void)imageZoom {
	[EXPhotoViewer showImageFrom:self.itemImage];
}

@end
