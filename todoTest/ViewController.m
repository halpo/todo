//
//  ViewController.m
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//


#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Todo List";
	UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
	bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BGTex"]];
	self.tableView.backgroundView = bgView;
    
    self.menuButton.target = self;
    self.menuButton.action = @selector(menu:);
    
	currentUser = [PFUser currentUser];
	if (!currentUser) {
		[self logout];
	}
	else {
		[self getList];
	}
}

- (void)getList {
	[Todo MR_truncateAll];
	PFQuery *query = [PFQuery queryWithClassName:@"Todo"];
	[query whereKey:@"user" equalTo:currentUser];
	NSArray *onlineObjects = [query findObjects];

	for (PFObject *obj in onlineObjects) {
		Todo *listItem = [Todo MR_createEntity];
		listItem.title = (NSString *)[obj valueForKey:@"title"];
		listItem.creationDate = (NSDate *)[obj valueForKey:@"creationDate"];
		listItem.isComplete = [[obj valueForKey:@"isComplete"] boolValue];
		listItem.theDescription = (NSString *)[obj valueForKey:@"theDescription"];
		PFFile *imageFile = [obj valueForKey:@"image"];
		NSData *imageData = [imageFile getData];
		listItem.image = imageData;
		listItem.parseID = (NSString *)[obj valueForKey:@"objectId"];
	}

	[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
	[self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
	if (!currentUser) {
		[self logout];
	}
	else {
		self.items = [Todo MR_findAllSortedBy:@"creationDate" ascending:NO];
		[self.tableView reloadData];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

#pragma mark TableView Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TodoCell"];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TodoCell"];
	}
	Todo *item = [self.items objectAtIndex:indexPath.row];
	UILabel *titleLabel = (UILabel *)[cell viewWithTag:1];
	UILabel *dateLabel = (UILabel *)[cell viewWithTag:2];
	NSDateFormatter *dateFormatter = [NSDateFormatter new];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	NSString *dateString = [dateFormatter stringFromDate:item.creationDate];
	titleLabel.text = item.title;
	dateLabel.text = dateString;
	item.isComplete ? [cell setAccessoryType:UITableViewCellAccessoryCheckmark] : [cell setAccessoryType:UITableViewCellAccessoryNone];
	iOS7 ? cell.tintColor = kGreenColour : nil;
	UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
	backView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
	cell.backgroundView = backView;
	cell.backgroundColor = [UIColor clearColor];
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	Todo *item = [self.items objectAtIndex:indexPath.row];
	[item MR_deleteEntity];
	[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
	self.items = [Todo MR_findAllSortedBy:@"creationDate" ascending:NO];
	[self.tableView reloadData];
}

- (NSString *)stringFromDate:(NSDate *)theDate {
	NSString *dateString;
	return dateString;
}

- (NSDate *)offsetDays:(int)days fromDate:(NSDate *)theDate {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setDay:days];
	NSDate *newDate = [gregorian dateByAddingComponents:offsetComponents toDate:theDate options:0];
	return newDate;
}

- (void)menu:(UIBarButtonItem *)button {
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"Menu" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Sync", @"Log out", nil];
    [menu showFromBarButtonItem:button animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self sync];
            break;
        case 1:
            [self logout];
            break;
            
        default:
            break;
    }
}

- (void)sync {
	if (!hud) {
		hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"Syncing Items...";
	}
	[hud show:YES];
	[self performSelector:@selector(theSync) withObject:nil afterDelay:0.01];
}

- (void)theSync {
	for (Todo *item in self.items) {
		NSDictionary *itemDict;
		if (item.image) {
			PFFile *imageFile = [PFFile fileWithName:@"image.jpg" data:item.image];
			itemDict = @{
				@"user":currentUser,
				@"creationDate":item.creationDate,
				@"title":item.title,
				@"isComplete":[NSNumber numberWithBool:item.isComplete],
				@"theDescription":item.theDescription,
				@"image":imageFile
			};
		}
		else {
			itemDict = @{
				@"user":currentUser,
				@"creationDate":item.creationDate,
				@"title":item.title,
				@"isComplete":[NSNumber numberWithBool:item.isComplete],
				@"theDescription":item.theDescription
			};
		}

		if (!item.parseID) {
			PFObject *newObject = [PFObject objectWithClassName:@"Todo" dictionary:itemDict];
			[newObject save];
			[item setParseID:[newObject objectId]];
			[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
		}
		else {
			PFQuery *query = [PFQuery queryWithClassName:@"Todo"];
			[query whereKey:@"user" equalTo:currentUser];
			PFObject *existingObject = [query getObjectWithId:item.parseID];
			[existingObject setValuesForKeysWithDictionary:itemDict];
			[existingObject saveInBackground];
		}
	}

	PFQuery *query = [PFQuery queryWithClassName:@"Todo"];
	[query whereKey:@"user" equalTo:currentUser];
	NSArray *onlineObjects = [query findObjects];
	for (PFObject *object in onlineObjects) {
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parseID == %@", [object valueForKey:@"objectId"]];
		Todo *fetchedItem = [Todo MR_findFirstWithPredicate:predicate];
		if (!fetchedItem) {
			[object deleteEventually];
		}
	}

	self.items = [Todo MR_findAllSortedBy:@"creationDate" ascending:NO];
	[hud hide:YES];
	[self.tableView reloadData];
}

- (void)logout {
    [PFUser logOut];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([[segue identifier] isEqualToString:@"ItemDetailSegue"]) {
		ItemDetailViewController *vc = [segue destinationViewController];
		NSIndexPath *indexpath = [self.tableView indexPathForSelectedRow];
		vc.item = [self.items objectAtIndex:indexpath.row];
	}
}

@end
