//
//  Todo.m
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "Todo.h"


@implementation Todo

@dynamic creationDate;
@dynamic isComplete;
@dynamic theDescription;
@dynamic title;
@dynamic image;
@dynamic parseID;


@end
