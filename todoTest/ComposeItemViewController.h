//
//  ComposeItemViewController.h
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTAssetsPickerController.h"

@interface ComposeItemViewController : UIViewController <UITextViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CTAssetsPickerControllerDelegate> {
	NSArray *selectedImage;
}
@property (strong, nonatomic) UIButton *imgButton;
@property (strong, nonatomic) IBOutlet SAMTextView *titleText;
@property (strong, nonatomic) IBOutlet SAMTextView *descriptionText;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@end
