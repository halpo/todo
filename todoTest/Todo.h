//
//  Todo.h
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Todo : NSManagedObject

@property (nonatomic, retain) NSDate *creationDate;
@property (nonatomic, assign) BOOL isComplete;
@property (nonatomic, retain) NSString *theDescription;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSData *image;
@property (nonatomic, retain) NSString *parseID;


@end
