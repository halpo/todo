//
//  constants.h
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#define iOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define kGreenColour [UIColor colorWithRed:0.363 green:0.753 blue:0.325 alpha:1]
#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height
