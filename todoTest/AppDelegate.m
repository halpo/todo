//
//  AppDelegate.m
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[MagicalRecord setupAutoMigratingCoreDataStack];
	[Parse setApplicationId:@"N9mLGNhrJYGUeQjOBnVQ3v3meCmubtI6pTaAZpqb"
	              clientKey:@"ik25YkdC4BMvLLCbb0ahJIYluaIVi6rfCOtyhrAQ"];
	[PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
	[[UINavigationBar appearance] setTintColor:kGreenColour];
	return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	PFInstallation *currentInstallation = [PFInstallation currentInstallation];
	[currentInstallation setDeviceTokenFromData:deviceToken];
	[currentInstallation saveInBackground];
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
	if (![[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
		[PFUser logOut];
	}
	[MagicalRecord cleanUp];
}

@end
