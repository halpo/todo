//
//  ItemDetailViewController.h
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EXPhotoViewer.h"

@interface ItemDetailViewController : UIViewController
@property (strong, nonatomic) Todo *item;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) UIImageView *doneImage;
@property (strong, nonatomic) IBOutlet UIImageView *itemImage;

- (IBAction)completePressed:(id)sender;

@end
