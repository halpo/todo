//
//  ComposeItemViewController.m
//  todoTest
//
//  Created by Paul on 05/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ComposeItemViewController.h"

@interface ComposeItemViewController ()

@end

@implementation ComposeItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.view layoutIfNeeded];
	self.title = @"New Item";
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Paper"]];
	self.titleText.layer.cornerRadius = 5.0f;
	self.titleText.placeholder = @"Title";
	self.titleText.layer.borderColor = [[UIColor lightGrayColor] CGColor];
	self.titleText.layer.borderWidth = 0.5;
	self.descriptionText.layer.cornerRadius = 5.0f;
	self.descriptionText.placeholder = @"Description";
	self.descriptionText.layer.borderColor = [[UIColor lightGrayColor] CGColor];
	self.descriptionText.layer.borderWidth = 0.5;

	self.imgButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.imgButton.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
	[self.imgButton setTitleColor:kGreenColour forState:UIControlStateNormal];
	[self.imgButton.titleLabel setNumberOfLines:0];
	[self.imgButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
	[self.imgButton setTitle:@"Choose\nImage" forState:UIControlStateNormal];
	[self.imgButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:17.0]];
	[self.imgButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
	[self.imgButton addTarget:self action:@selector(chooseImage:) forControlEvents:UIControlEventTouchUpInside];

	float size;
	if (kScreenHeight < 568) {
		size = 79.0;
	}
	else {
		size = 167.0;
	}

	[self.imgButton setFrame:CGRectMake(kScreenWidth * 0.5 - size * 0.5, self.descriptionText.frame.origin.y + self.descriptionText.frame.size.height + 20, size, size)];
	[self.view addSubview:self.imgButton];

	[self.saveButton addTarget:self action:@selector(savePressed) forControlEvents:UIControlEventTouchUpInside];

	[self.navigationItem.rightBarButtonItem setEnabled:NO];
	self.navigationItem.rightBarButtonItem.target = self;
	self.navigationItem.rightBarButtonItem.action = @selector(donePressed);

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
}

- (void)keyboardDidShow {
	[self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)keyboardDidHide {
	[self.navigationItem.rightBarButtonItem setEnabled:NO];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
	if (kScreenHeight < 568) {
		[UIView animateWithDuration:0.2 animations: ^{
		    self.view.frame = CGRectOffset(self.view.frame, 0, -70);
		}];
	}
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	if (kScreenHeight < 568) {
		[UIView animateWithDuration:0.2 animations: ^{
		    self.view.frame = CGRectOffset(self.view.frame, 0, 70);
		}];
	}
}

- (void)donePressed {
	[self.view endEditing:YES];
}

- (void)chooseImage:(UIButton *)btn {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Library", nil];
	[actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0: {
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
				[self cameraImage];
			}
			else {
				UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"No camera on this device" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCameraAlert show];
			}
			break;
		}

		case 1:
			[self libraryImage];
			break;

		default:
			break;
	}
}

- (void)libraryImage {
	CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
	picker.delegate = self;
	picker.assetsFilter = [ALAssetsFilter allPhotos];
	picker.selectedAssets = [NSMutableArray arrayWithArray:selectedImage];
	[self presentViewController:picker animated:YES completion:nil];
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
	[picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	UIImage *img = [UIImage imageWithCGImage:[[assets[0] defaultRepresentation] fullResolutionImage]];
	[self.imgButton setImage:img forState:UIControlStateNormal];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset {
	if (picker.selectedAssets.count >= 1) {
		return NO;
	}
	else {
		return YES;
	}
}

- (void)cameraImage {
	UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
	imagePicker.delegate = self;
	imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
	imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
	imagePicker.allowsEditing = YES;
	[self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
	[self.imgButton setImage:image forState:UIControlStateNormal];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)savePressed {
	UIAlertView *saveAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	if (!self.titleText.text.length || !self.descriptionText.text.length) {
		saveAlert.title = @"Incomplete";
		saveAlert.message = @"Please complete all fields";
		[saveAlert show];
	}
	else {
		Todo *new = [Todo MR_createEntity];
		new.title = self.titleText.text;
		new.theDescription = self.descriptionText.text;
		new.creationDate = [NSDate date];
		NSData *imageData = UIImageJPEGRepresentation(self.imgButton.imageView.image, 0.1);
		if (imageData) {
			new.image = imageData;
		}
		[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

@end
