//
//  LoginViewController.m
//  todoTest
//
//  Created by Paul on 15/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.passField setSecureTextEntry:YES];
	[self.loginButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
	[self.registerButton addTarget:self action:@selector(registerPress) forControlEvents:UIControlEventTouchUpInside];
    [self.rememberSwitch addTarget:self action:@selector(rememberChanged:) forControlEvents:UIControlEventValueChanged];
	PFUser *currentUser = [PFUser currentUser];
	if (currentUser) {
		[self performSegueWithIdentifier:@"LoginSegue" sender:nil];
	}
}

- (void)viewDidAppear:(BOOL)animated {
    self.userField.text = @"";
    self.passField.text = @"";
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
        self.userField.text = [SSKeychain passwordForService:@"user" account:@"login"];
        self.passField.text = [SSKeychain passwordForService:@"pass" account:@"login"];
        [self.rememberSwitch setOn:YES];
    } else {
        [self.rememberSwitch setOn:NO];
    }
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)login {
	if (self.userField.text.length && self.passField.text.length) {
		[PFUser logInWithUsernameInBackground:self.userField.text password:[self hashed:self.passField.text] block: ^(PFUser *user, NSError *error) {
		    if (!error) {
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
                    [SSKeychain setPassword:self.userField.text forService:@"user" account:@"login"];
                    [SSKeychain setPassword:self.passField.text forService:@"pass" account:@"login"];
                }
		        [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
			}
		    else {
		        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[NSString stringWithFormat:@"Login Error!\n%@", [error localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		        [errorAlert show];
			}
		}];
	}
	else {
		[self validationError];
	}
}

- (void)registerPress {
	if (self.userField.text.length && self.passField.text.length) {
		if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
			[SSKeychain setPassword:self.userField.text forService:@"user" account:@"login"];
            [SSKeychain setPassword:self.passField.text forService:@"pass" account:@"login"];
		}
		PFUser *user = [PFUser user];
		user.username = self.userField.text;
		user.password = [self hashed:self.passField.text];
		[user signUpInBackgroundWithBlock: ^(BOOL succeeded, NSError *error) {
		    if (!error) {
		        NSLog(@"Success!");
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
                    [SSKeychain setPassword:self.userField.text forService:@"user" account:@"login"];
                    [SSKeychain setPassword:self.passField.text forService:@"pass" account:@"login"];
                }
		        [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
			}
		    else {
		        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Registration Error" message:[NSString stringWithFormat:@"Registration Error!\n%@", [error localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		        [errorAlert show];
			}
		}];
	}
	else {
		[self validationError];
	}
}

- (void)rememberChanged:(UISwitch *)swch {
	if (swch.isOn) {
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"remember"];
	}
	else {
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"remember"];
        [SSKeychain deletePasswordForService:@"pass" account:@"main"];
        [SSKeychain deletePasswordForService:@"user" account:@"main"];
	}
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)validationError {
	UIAlertView *validationAlert = [[UIAlertView alloc] initWithTitle:@"Incomplete Fields" message:@"Please complete the login fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[validationAlert show];
}

- (IBAction)hideKeyboard:(id)sender {
	[self.view endEditing:YES];
}

- (NSString *)hashed:(NSString *)input {
	const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
	NSData *data = [NSData dataWithBytes:cstr length:input.length];
	uint8_t digest[CC_SHA256_DIGEST_LENGTH];
	CC_SHA256(data.bytes, (int)data.length, digest);
	NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
	for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
		[output appendFormat:@"%02x", digest[i]];
	}
	return output;
}

@end
