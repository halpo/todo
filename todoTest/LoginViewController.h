//
//  LoginViewController.h
//  todoTest
//
//  Created by Paul on 15/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>
#import "SSKeychain.h"

@interface LoginViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *loginBox;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UITextField *userField;
@property (strong, nonatomic) IBOutlet UITextField *passField;
@property (strong, nonatomic) IBOutlet UISwitch *rememberSwitch;


- (IBAction)hideKeyboard:(id)sender;

@end
